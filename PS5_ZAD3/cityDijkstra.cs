﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PS5_ZAD3
{
    class cityDijkstra
    {
        public int id;
        public int size;
        public double[,] dijkstraTime = { };
        public List<int> unvisited = new List<int>();
       

        public cityDijkstra(int id, int size)
        {
            this.id = id;
            this.size = size;
            dijkstraTime = new double[2, size + 1];
            
        }


        public void UpdateDijkstra(ref Connection[,] connections)
        {
            
            int i, j;
            double min = 9999;
            int minPosition = id;  
            
            
            for (j = 1; j < size + 1; j++) //wstepne wypelnienie
            {
                if (connections[id, j] == null) dijkstraTime[0, j] = 999;
                else
                {
                    dijkstraTime[0, j] = connections[id, j].GetTime();
                    dijkstraTime[1, j] = id;
                    
                }

                if (j == id) dijkstraTime[0, j] = 0;

                unvisited.Add(j);
            }
            unvisited.Remove(minPosition);

            
            foreach (int c in unvisited.ToList())
            {
                
                min = 9999;
                for (i = 1; i < size + 1; i++)//szukanie minimum
                {
                    if (dijkstraTime[0, i] < min && unvisited.Contains(i))
                    {
                        min = dijkstraTime[0, i];
                        minPosition = i;
                        
                    }
                }
               //Console.WriteLine("new min value: " + min + " on position " + minPosition + " " );


                
                for (i = 1; i < size + 1; i++)
                {
                    
                    if (unvisited.Contains(i) && connections[minPosition, i] != null)
                    {
                        
                        if (connections[minPosition, i].GetTime() + dijkstraTime[0, minPosition] + 5 < dijkstraTime[0, i])
                        {
                            dijkstraTime[0, i] = connections[minPosition, i].GetTime() + dijkstraTime[0, minPosition] + 5;
                            dijkstraTime[1, i] = minPosition;
                        }
                    }
                }
                //Console.WriteLine("usuwam " + minPosition);
                
                unvisited.Remove(minPosition);

            }
          }
        


        public void PrintDijkstra()
        {
            int i, j;
            Console.WriteLine("Shortest Paths for city {0}", id);

            for (i = 0; i < 2; i++)
            {
                if (i == 0) Console.Write("M ");
                else Console.Write("P ");

                for (j = 1; j < size + 1; j++)
                {
                    Console.Write(dijkstraTime[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }



    }
}

