﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS5_ZAD3
{
    class City
    {
        public int id;
        public bool wasVisited;

        public City(int id)
        {
            this.id = id;
            wasVisited = false;
        }
    }
}
