﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS5_ZAD3
{
    class Question
    {
        public int m1;
        public int m2;
        public int timeToBeat;
        public int answer;
        
        public Question(int m1, int m2, int timeToBeat)
        {
            this.m1 = m1;
            this.m2 = m2;
            this.timeToBeat = timeToBeat;
            this.answer = -1;
        }
    }
}
