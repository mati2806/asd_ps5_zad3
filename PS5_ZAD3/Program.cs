﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PS5_ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "maly3.txt";
            Connection [,] connections = new Connection[,] { };



            try { LoadFile(fileName, ref connections); }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie znaleziono pliku!");
            }



        }

        static void LoadFile(string fileName, ref Connection [,] connections)
        {
            int i = 3;
            int date;
            int size;
            double time = 0;
            bool alreadyExists;
            cityDijkstra newCity;
            List<Question> questions = new List<Question>();
            List<cityDijkstra> listOfCities = new List<cityDijkstra>();

            StreamReader reader = new StreamReader(fileName);

            string[] words = new string[] { };

            words = reader.ReadToEnd().Split(new[] { '\n', ' ', '-' });


            Console.WriteLine("Test: {0}", fileName);
            Console.WriteLine("Miast: " + words[0]);
            Console.WriteLine("Polaczen: " + words[1]);
            Console.WriteLine("Zapytan: " + words[2]);
            Console.WriteLine("=====================");
            Console.WriteLine();

            size = Convert.ToInt32(words[0]);
            connections = new Connection[size+1,size+1];
            


            

           
            Console.WriteLine();

            
            i = 3;

            while (i < words.Length) //skip do polecen
            {

                if (words[i + 3] == "b")
                {                   

                    i = i + 8;
                }
                else if (words[i + 3] == "m")
                {

                    i = i + 7;
                }
                else
                {
                    break;
                }
                //Console.WriteLine();
            }

            while (i < words.Length - 2)
            {
                Console.WriteLine("CZY DA SIE DOJECHAC Z {0} DO {1} W {2} MINUT", Convert.ToInt32(words[i]), Convert.ToInt32(words[i + 1]), Convert.ToInt32(words[i + 2]));
                questions.Add(new Question(Convert.ToInt32(words[i]), Convert.ToInt32(words[i + 1]), Convert.ToInt32(words[i + 2])));
                i = i + 3;
            }

            reader.Close();
            reader = new StreamReader(fileName);

            i = 3;

            while (i < words.Length)
            {

                if (words[i + 3] == "b")
                {
                    date = Convert.ToInt32(words[i] + words[i + 1] + words[i + 2]);
                    Console.WriteLine("BUDOWA DATE:{0} P:{1} M1:{2} M2:{3} AVG_SPD:{4} DIS:{5}", date, words[i + 3], words[i + 4], words[i + 5], words[i + 6], words[i + 7]);
                    time = (Convert.ToDouble(words[i + 7]) / Convert.ToDouble(words[i + 6])) * 60;
                    //Console.WriteLine("TRAVEL_TIME:{0} MINUTES", time);


                    newCity = new cityDijkstra(Convert.ToInt32(words[i + 4]), size);

                    alreadyExists = false;

                    foreach (Question q in questions)//lista miast potrzebnych do odpowiedzi
                    {
                        if(q.m1 == newCity.id)
                        {
                            foreach (cityDijkstra c in listOfCities)
                            {
                                if (c.id == newCity.id)
                                {
                                    alreadyExists = true;
                                    break;
                                }

                            }
                            if (alreadyExists == false) listOfCities.Add(newCity);
                        }

                    }

                    newCity = new cityDijkstra(Convert.ToInt32(words[i + 5]), size);
                    alreadyExists = false;

                    foreach (Question q in questions)//lista miast potrzebnych do odpowiedzi
                    {
                        if (q.m1 == newCity.id)
                        {
                            foreach (cityDijkstra c in listOfCities)
                            {
                                if (c.id == newCity.id)
                                {
                                    alreadyExists = true;
                                    break;
                                }

                            }
                            if (alreadyExists == false) listOfCities.Add(newCity);
                        }

                    }


                    //Console.WriteLine();

                    connections[Convert.ToInt32(words[i + 4]), Convert.ToInt32(words[i + 5])] = new Connection(date, Convert.ToInt32(words[i + 7]), Convert.ToInt32(words[i + 6]), Convert.ToInt32(words[i + 4]));
                    connections[Convert.ToInt32(words[i + 5]), Convert.ToInt32(words[i + 4])] = new Connection(date, Convert.ToInt32(words[i + 7]), Convert.ToInt32(words[i + 6]), Convert.ToInt32(words[i + 5]));


                    //Console.WriteLine(date);
                    //PrintTable(ref connections, size);


                    foreach (Question q in questions) //wyswietl tylko miasta z zapytania
                    {
                        
                        
                        foreach (cityDijkstra c in listOfCities)
                        {
                            if (c.id == q.m1 && q.answer==-1)
                            {
                                c.UpdateDijkstra(ref connections);
                                c.PrintDijkstra();

                                //Console.WriteLine("Porownuje {0} do {1}", c.dijkstraTime[0, q.m2], q.timeToBeat);
                                if (c.dijkstraTime[0, q.m2] < q.timeToBeat)
                                {
                                    q.answer = date;
                                }
                            }
                        }
                        
                    }


                    i = i + 8;
                }
                else if (words[i + 3] == "m")
                {
                    date = Convert.ToInt32(words[i] + words[i + 1] + words[i + 2]);
                    Console.WriteLine("POPRAWA DATE:{0} P:{1} M1:{2} M2:{3} AVG_SPD:{4}", date, words[i + 3], words[i + 4], words[i + 5], words[i + 6]);

                    connections[Convert.ToInt32(words[i + 4]), Convert.ToInt32(words[i + 5])].SetSpeed(Convert.ToInt32(words[i + 6]));
                    connections[Convert.ToInt32(words[i + 4]), Convert.ToInt32(words[i + 5])].SetDate(date);

                    connections[Convert.ToInt32(words[i + 5]), Convert.ToInt32(words[i + 4])].SetSpeed(Convert.ToInt32(words[i + 6]));
                    connections[Convert.ToInt32(words[i + 5]), Convert.ToInt32(words[i + 4])].SetDate(date);

                    //Console.WriteLine(date);
                    //PrintTable(ref connections, size);

                    //Console.WriteLine();


                    foreach (Question q in questions) //wyswietl tylko miasta z zapytania
                    {

                        
                        foreach (cityDijkstra c in listOfCities)
                        {
                            if (c.id == q.m1 && q.answer == -1)
                            {
                                c.UpdateDijkstra(ref connections);
                                c.PrintDijkstra();

                                //Console.WriteLine("Porownuje {0} do {1}", c.dijkstraTime[0, q.m2], q.timeToBeat);
                                if (c.dijkstraTime[0, q.m2] < q.timeToBeat)
                                {
                                    q.answer = date;
                                }
                            }
                        }

                    }

                    i = i + 7;
                }
                else
                {
                    break;                
                }

                
                
                //Console.WriteLine();

            }
            GetAnswers(ref questions);


        }

        static void GetAnswers(ref List<Question> questions)
        {
            Console.WriteLine();
            foreach(Question q in questions)
            {
                Console.Write("Z {0} do {1} ponizej {2}min: ", q.m1, q.m2, q.timeToBeat);
                if (q.answer != -1) Console.Write(q.answer);
                else Console.Write("NIE");
                Console.WriteLine();
            }


        }

        static void PrintTable(ref Connection [,] cities, int size)
        {
            int i = 0;
            int j = 0;
          

            Console.WriteLine("TIME:");
            for(i=1; i<=size; i++)
            {
                Console.Write(i + " ");
                for(j=1; j<=size; j++)
                {
                    if (i == j)
                        Console.Write("0 ");
                    else if (cities[j, i] == null)
                        Console.Write("INF ");
                   
                    else
                        Console.Write(cities[j, i].GetTime() + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();
      

        }

       
    }
}
