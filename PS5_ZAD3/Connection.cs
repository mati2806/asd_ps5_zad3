﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS5_ZAD3
{
    class Connection
    {
        public int date;
        public int id;
        public double distance;
        public double averageSpeed;
        
       
        


        public Connection (int date, double distance, double averageSpeed, int id)
        {
            this.id = id;
            this.date = date;
            this.distance = distance;
            this.averageSpeed = averageSpeed;
            
            
        }

        public void SetSpeed(double averageSpeed)
        {
            this.averageSpeed = averageSpeed;
        }

        public void SetDate(int date)
        {
            this.date = date;
        }

        public double GetTime()
        {
            double time;
            time = (distance / averageSpeed) * 60;

            return time;
        }

    

      

       
    }
}
